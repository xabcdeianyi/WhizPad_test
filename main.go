package main

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"math/rand"
	"time"
)

const bedPayload10 = `{"dataType": 2, "event": 4, "sensors": "111111111111111111111111111111", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload1 = `{"dataType": 1, "event": 0, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload2 = `{"dataType": 1, "event": 1, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload3 = `{"dataType": 1, "event": 2, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload4 = `{"dataType": 1, "event": 3, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload5 = `{"dataType": 1, "event": 4, "sensors": "111111111111111111111111111111", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload6 = `{"dataType": 1, "event": 3, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload7 = `{"dataType": 1, "event": 2, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload8 = `{"dataType": 1, "event": 1, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`
const bedPayload9 = `{"dataType": 1, "event": 0, "sensors": "000000000000000000000000000000", "activity": 0, "activityTop": 0, "activityMiddle": 0, "activityDown": 0, "activity_1min": 0, "sleepStatus": 2, "pressedTime": 0}`

//const testId = "WhizPad_RC-DE6BBF7EF5C0"

var testIds = []string{
	"WhizPad_RC-000000000001",
	"WhizPad_RC-000000000002",
	"WhizPad_RC-000000000003",
	"WhizPad_RC-000000000004",
	"WhizPad_RC-000000000005",
	"WhizPad_RC-000000000006",
	"WhizPad_RC-000000000007",
	"WhizPad_RC-000000000008",
	"WhizPad_RC-000000000009",
	"WhizPad_RC-00000000000A",
}

const Online = `{  "online" : 1, "firmwareVer" : 5.1, "battery" : 0.71, "gateway" : { "type" : "WhizConnect", "id" : "WhizConnect-0008DCDE9FEE" } }`

func main() {

	opt := getMqttOpt("tcp://34.81.144.96:1883", "testDev")
	client := mqtt.NewClient(opt)
	client.Connect()
	select {}
}
func getMqttOpt(url, clientId string) *mqtt.ClientOptions {
	opt := mqtt.NewClientOptions().AddBroker(url)
	opt.ClientID = clientId
	opt.AutoReconnect = true
	opt.CleanSession = true
	opt.Order = true
	opt.ConnectTimeout = 30 * time.Second
	opt.MaxReconnectInterval = 10 * time.Minute
	opt.OnConnect = onConnect
	return opt
}
func send(client mqtt.Client, testId string) {
	i := 1
	for {
		if i == 10 {
			client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload10)
			i = 0
		} else {
			switch i {
			case 1:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload1)
			case 2:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload2)
			case 3:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload3)
			case 4:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload4)
			case 5:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload5)
			case 6:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload6)
			case 7:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload7)
			case 8:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload8)
			case 9:
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/sensor", testId), 0, false, bedPayload9)

			}
		}
		i++
		fmt.Println("sensor:", i)
		time.Sleep((time.Duration(rand.Int63n(400)) * time.Millisecond) + (800 * time.Millisecond))
	}
}
func onConnect(client mqtt.Client) {
	for _, id := range testIds {
		go send(client, id)
	}
	go func() {
		for {
			for _, id := range testIds {
				client.Publish(fmt.Sprintf("G-Tech/WhizPad/%s/online", id), 0, false, Online)
			}
			fmt.Println("online")
			time.Sleep(1 * time.Minute)
		}
	}()
}
