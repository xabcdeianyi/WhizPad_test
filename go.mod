module gitlab.kalenet.info/seda-gtech/testBed

go 1.16

require (
	github.com/eclipse/paho.mqtt.golang v1.4.2
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/net v0.9.0 // indirect
)
